import smtplib
import string
import random
from email.mime.text import MIMEText
def send_email(message, subject, toaddrs):
    fromaddr = 'email adress here'
    username = 'user name here'
    password = 'password here'
    msg = MIMEText(message, 'html')
    msg['Subject']  = subject
    msg['From'] = fromaddr
    msg['Reply-to'] = 'no-reply'
    msg['To'] = toaddrs
    server = smtplib.SMTP('smtp server here')
    server.starttls()
    server.login(username, password)
    server.sendmail(fromaddr, [toaddrs], msg.as_string())
    server.quit()
rand = string.ascii_lowercase
killme = ['exit', 'exit()', 'quit', 'quit()', 'killme', 'die', 'kill']
while True:
    try:
        print('Enter a killme word to quit.')
        message = raw_input('Enter your mesage:\n')
        if message in killme:
            print('dying...')
            quit()
        toaddrs = raw_input('Enter receiver email address:\n')
        if toaddrs in killme:
            print('dying...')
            quit()
        numofmessages = raw_input('Enter either the number of messages you want or "infinite":\n')
        if numofmessages in killme:
            print('dying...')
            quit()
    except(NameError):
        message = input('Enter your message:\n')
        if message in killme:
            print('dying...')
            quit()
        toaddrs = input('Enter receiver email address:\n')
        if toaddrs in killme:
            print('dying...')
            quit()
        numofmessages = input('Enter either the number of messages you want or "infinite":\n')
        if numofmessages in killme:
            print('dying...')
            quit()
    if numofmessages.lower() == 'infinite':
        while True:
            a = str(rand[random.randrange(len(rand))]) + 'umbus '
            random.seed(random.randrange(100))
            b = str(rand[random.randrange(len(rand))]) + 'umbus '
            random.seed(random.randrange(100))
            c = str(rand[random.randrange(len(rand))]) + 'umbus '
            random.seed(random.randrange(100))
            d = str(rand[random.randrange(len(rand))]) + 'umbus.'
            subject = str(a + b + c + d)
            send_email(str(message), str(subject), str(toaddrs))
        print('This shouldn\'t ever print...')
        continue
    try:
        for x in range(int(numofmessages)):
            a = str(rand[random.randrange(len(rand))]) + 'umbus '
            random.seed(random.randrange(100))
            b = str(rand[random.randrange(len(rand))]) + 'umbus '
            random.seed(random.randrange(100))
            c = str(rand[random.randrange(len(rand))]) + 'umbus '
            random.seed(random.randrange(100))
            d = str(rand[random.randrange(len(rand))]) + 'umbus.'
            subject = str(a + b + c + d)
            send_email(str(message), str(subject), str(toaddrs))
        print('Sent ' + str(numofmessages) + 'messages to ' + str(toaddrs))
        continue
    except(TypeError):
        print('Error not correct input for number of messages.')
        continue
